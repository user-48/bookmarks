# bookmarks

Все нужные закладки всегда под рукой, без использования специальных сервисов и программ.

![Screenshot](https://gitlab.com/user-48/bookmarks/raw/master/scr.png "Project preview")

Легко добавить, менять нужное название и ссылку, в любом текстовом редакторе.

